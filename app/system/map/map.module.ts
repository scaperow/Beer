/*
配置功能模块
*/

import {ModuleWithProviders, NgModule, Optional, SkipSelf}       from '@angular/core';
import {mapRouting} from './map.routing';
import {CommonModule} from '@angular/common';
import {ButtonModule, DataTableModule, SharedModule, FieldsetModule, DropdownModule} from 'primeng/primeng';
import { FormsModule }   from '@angular/forms';
import {MapMonitorComponent} from './monitor.component';
import {CoreModule} from '../../core/core.module'
import {MapsComponent} from './maps.component';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [RouterModule,ButtonModule, CommonModule, CoreModule, mapRouting, DataTableModule, SharedModule, FieldsetModule, DropdownModule, FormsModule],
    declarations: [MapMonitorComponent,MapsComponent],
    exports: []
})

export class MapModule {

}
