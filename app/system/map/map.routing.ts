import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MapMonitorComponent }      from './monitor.component';
import {MapsComponent} from './maps.component';
const routing: Routes = [
    {
        path: '',
        component: MapsComponent,
        children: [
            {
                path: 'monitor',
                component: MapMonitorComponent,
                pathMatch: 'full'
            }
        ]

    }

];

export const mapRouting: ModuleWithProviders = RouterModule.forChild(routing);
