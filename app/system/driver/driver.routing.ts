import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DriverEditorComponent }      from './editor.component';
import { DriverManagerComponent }      from './manager.component';
import {DriverComponent} from './driver.component';

const routing: Routes = [
    {
        path: '',
        component: DriverComponent,
        children: [
            {
                path: 'editor',
                component: DriverEditorComponent,
                pathMatch: 'full'
            },
            {
                path: 'manager',
                component: DriverManagerComponent,
                pathMatch: 'full'
            }
        ]

    }

];

export const driverRouting: ModuleWithProviders = RouterModule.forChild(routing);
