import {Component} from '@angular/core';

@Component({
    selector:'be-driver',
    template:`<router-outlet></router-outlet>`
})
export class DriverComponent{
    
}