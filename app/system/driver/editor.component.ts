import { Component } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpModule } from '@angular/http';

@Component({
    selector: 'driver-editor',
    templateUrl: 'dist/app/system/driver/editor.component.html'
})

export class DriverEditorComponent {
    title = 'Tour of Heroes';

    theForm: FormGroup;

    constructor(private fb: FormBuilder) {
        this.theForm = fb.group({
            name: [''],
            birthday: [''],
            IDCard: [''],
            status: ['']
        });
    }
}
