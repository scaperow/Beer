import { Component } from '@angular/core';
import {InputTextModule} from 'primeng/primeng';
import {DRIVERS} from './driver.mock';
import {ManageComopnent} from '../../core/manage/manage.component';


@Component({
    selector: 'driver-manager',
    templateUrl: 'dist/app/system/driver/manager.component.html'
}) 
 
export class DriverManagerComponent {  
    //drivers = DRIVERS;
    carTypes = [{ label: '请选择', value: null },
            { label: 'A', value: 'A' }];

    searchModel = {
        driverName: 'abc',
        tel: '1323',
        carType: 'A',
    };
  
    drivers;
}
