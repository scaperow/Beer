/*
配置功能模块
*/

import { ModuleWithProviders, NgModule, Optional, SkipSelf, Pipe, PipeTransform } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DriverEditorComponent } from './editor.component';
import { DriverManagerComponent } from './manager.component';
import { DriverComponent } from './driver.component';
import { driverRouting } from './driver.routing';
import { ButtonModule, DataTableModule, SharedModule, RadioButtonModule, FieldsetModule, CalendarModule, DropdownModule, InputTextModule } from 'primeng/primeng';
import { DRIVERS } from './driver.mock';
import { CoreModule } from '../../core/core.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { Http, Response } from '@angular/http';

@NgModule({
    imports: [driverRouting, RadioButtonModule, RouterModule, ReactiveFormsModule, CommonModule, CalendarModule, CoreModule, ButtonModule, InputTextModule, DataTableModule, SharedModule, FieldsetModule, DropdownModule, FormsModule],
    declarations: [DriverEditorComponent, DriverManagerComponent, DriverComponent],
    exports: []
})

export class DriverModule {

}

