import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    styleUrls: ['app/app.component.css'],
    template: `
                <be-login></be-login>
                <be-layout>
                    <div class="content">
                        <router-outlet></router-outlet>
                    </div>
                </be-layout>`
})
export class AppComponent {
    title = 'Tour of Heroes';
}
