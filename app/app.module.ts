import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";

// Imports for loading & configuring the in-memory web api

import { AppComponent } from "./app.component";
import { routing } from "./app.routing";
import { HighlightDirective } from "./highlight.directive";
import { CoreModule } from "./core/core.module";
import {WelcomeComponent} from "./welcome.component";

@NgModule({
  imports: [
    CoreModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
  ],
  declarations: [
    WelcomeComponent,
    AppComponent,
    HighlightDirective,
  ],
  providers: [
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
