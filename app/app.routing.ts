import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { WelcomeComponent } from "./welcome.component";

const appRoutes: Routes = [
    {
        path: "",
        redirectTo: "/welcome",
        pathMatch: "full",
    },
    {
        path: "welcome",
        component: WelcomeComponent
    },
    {
        path: "driver",
        loadChildren: "app/system/driver/driver.module#DriverModule",
    },
    {
        path: "map",
        loadChildren: "app/system/map/map.module#MapModule",
    },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
