import {Component, ElementRef, AfterViewInit} from '@angular/core';


declare var BMap: any;

@Component({
    selector: 'be-map',
    template: '<div class="full" style="width:100%;min-height:500px;height:100%" id="{{id}}">正在加载地图 ...</div>'
})

export class MapComponent implements AfterViewInit {
    id = `map_${new Date().getTime()}`;
    maplet;

    constructor() {

    }

    ngAfterViewInit() {
        if (this.maplet == null) {
            this.maplet = new BMap.Map(this.id);

            this.maplet.centerAndZoom(new BMap.Point(121.4802370000, 31.2363050000), 8);
            this.maplet.enableScrollWheelZoom();
        }
    }
}