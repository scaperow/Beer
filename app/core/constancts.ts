export default class Constants {
    public static serviceUri = 'http://localhost:3334';
    public static loginUri = `${Constants.serviceUri}/session`;
}