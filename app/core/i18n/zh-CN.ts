export const Validator = {
    isNotNull: "必填",
    isEmail: "请输入正确的邮件格式",
    isNull: "不能有值",
    isDate: "请输入正确的日期格式",
    contains: "需要包含指定的内容",
    isFullWidth: "长度不符合规定",
};

export const Calendar = {
    firstDayOfWeek: 0,
    dayNames: ["星期一", "星期二", "星期三", "星期四", "星期五", "星期六", "星期日"],
    dayNamesShort: ["周一", "周二", "周三", "周四", "周五", "周六", "周日"],
    dayNamesMin: ["一", "二", "三", "四", "五", "六", "日"],
    monthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
    monthNamesShort: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
};
