import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'be-layout',
    styleUrls: ['dist/app/core/layout/layout.component.css'],
    templateUrl: 'dist/app/core/layout/layout.component.html'
})
export class LayoutCompoent {
    title = 'Tour of Heroes';
}
