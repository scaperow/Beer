import { Component, Input, Output, ViewEncapsulation, EventEmitter, transition, style, trigger, animate, state } from '@angular/core';
import { InputTextModule } from 'primeng/primeng';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import Constants from '../constancts';
import { Message } from 'primeng/primeng';
import { UserService } from './session.service';

@Component({
    selector: 'be-login',
    templateUrl: 'dist/app/core/session/login.component.html',
    styleUrls: ['dist/app/core/session/login.component.css'],
    animations: [
        trigger('showup', [
           // state('in', style({ opacity: 1, transform: 'translateY(0)' })),
            // state('out', style({ opacity: 1, transform: 'translateY(-100%)' })),
            /*
            transition('* => true', [
                style({
                    opacity: 100,
                    transform: 'translateY(-100%)'
                })
            ]),
            transition('true => *', [
                style({
                    opacity: 0,
                    transform: 'translateY(100%)'
                })
            ])
            */
        ])
    ]
})

export class LoginComponent {
    form: FormGroup;
    message: Message[] = [];
    showup: Boolean = true;

    constructor(private http: Http, private builder: FormBuilder) {
        this.form = builder.group({
            name: [''],
            password: [''],
            code: ['']
        });

        this.message.push({ detail: '假登录' });
        this.message.push({ detail: '假登录' });
        this.message.push({ detail: '假登录' });
    }

    login() {
        this
            .http
            .post(Constants.loginUri, this.form.value, {})
            .map(res => res.json())
            .subscribe(
            result => {
                if (result.success) {
                    UserService.setSession(result);

                    this.showup = false;
                    this.message.push({ summary: '登录成功' });
                } else {
                    this.message.push({ severity: 'error', summary: '登录失败', detail: result.message || '登录失败' });
                }
            }, error => {
                this.message.push({ severity: 'error', summary: '登录失败', detail: '登录异常' });
            });
    }
}