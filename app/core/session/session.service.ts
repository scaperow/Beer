import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Headers } from '@angular/http';

@Injectable()
export class UserService {
    static user: any;

    constructor(private http: Http) {
        this.http = http;
    }

    static setSession(user) {
        UserService.user = user;
    }

    static clearSession() {
        UserService.user = null;
    }
}