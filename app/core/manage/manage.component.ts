import { Component, Input, Output, ViewEncapsulation, EventEmitter } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/Rx' ;
import { Observable }     from 'rxjs/Observable';
 
@Component({
    selector: 'be-manage',
    styleUrls: ['dist/app/core/manage/manage.component.css'],
    templateUrl: 'dist/app/core/manage/manage.component.html'
})

export class ManageComopnent {
    @Input() manageName = null;// 对象名称
    @Input() canSearch = true;// 是否可以检索
    @Input() searchModel = {};
    @Input() searchResult = null;
    @Output() searchResultChange = new EventEmitter<any>();

    constructor(private http: Http) {
        //console.log(this.manageName);
        // this.search();
    }
    // 新建
    create() {

    }

    // 修改
    modify() {

    }

    // 删除
    delete() {

    }

    // 查询
    search(validate = true) {
        //this.searchResult = DRIVERS;
        //this.searchResultChange.emit(this.searchResult);
        //console.log(this.http);
        this
            .http 
            .get("http://localhost:3334/session")
            .map( r=>console.log(r));

        //console.log('trigged');
        /*
        console.log(`should post http request to Url is ROOTDOMAIN(should be in config file)/${this.manageName}/list with args which is ${JSON.stringify(this.searchModel)}`);
        if (validate) {
            // 验证
        }

        this.searchResult = DRIVERS;
        this.searchResultChange.emit(this.searchResult);
        */
    }
}