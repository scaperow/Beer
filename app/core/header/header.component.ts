import { Component } from '@angular/core';


@Component({
    selector: 'be-header',
    templateUrl: 'dist/app/core/header/header.component.html',
    styleUrls:['dist/app/core/header/header.component.css']
})
export class HeaderCompoent {
    title = 'Tour of Heroes';
}
