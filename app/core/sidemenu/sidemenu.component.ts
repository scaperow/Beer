import { Component } from '@angular/core';
import {AccordionModule} from 'primeng/primeng';

@Component({
    selector: 'be-sidemenu',
    templateUrl: 'dist/app/core/sidemenu/sidemenu.component.html'
})
export class SideMenuComponent {
    title = 'Tour of Heroes';
}
