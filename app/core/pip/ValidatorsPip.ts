import { Pipe, PipeTransform } from "@angular/core";
import * as _ from "underscore";
//import * as i18n from "i8n";

@Pipe({ name: "validators", pure: false })
export class ValidatorsPip implements PipeTransform {
    private static ValidateTips =
    {
        isNotNull: "必填",
        isEmail: "请输入正确的邮件格式",
        isNull: "不能有值",
        isDate: "请输入正确的日期格式",
        contains: "需要包含指定的内容",
        isFullWidth: "长度不符合规定",
    };

    public transform(value: any): any {
        if (value.dirty) {
            if (value.valid) {
                return "";
            } else {
                // 按照 validateTips 内的字段定义顺序来显示错误信息
                // 默认只显示第一条
                let errors = _.intersection(_.keys(ValidatorsPip.ValidateTips), _.keys(value.errors));

                if (!_.isEmpty(errors)) {
                    return `<span class="ui-message ui-messages-error ui-corner-all">
                                ${ValidatorsPip.ValidateTips[errors[0]] || errors[0]}
                            </span>`;
                }
            }
        }

        return "";
    }
}
