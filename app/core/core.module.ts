import {
    ModuleWithProviders, NgModule,
    Optional, SkipSelf
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule, InputTextModule, DataTableModule, SharedModule, AccordionModule } from 'primeng/primeng';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MapComponent } from './map/map.component';
import { ManageComopnent } from './manage/manage.component';
import { LoginComponent } from './session/login.component';
import { HeaderCompoent } from './header/header.component';
import { LayoutCompoent } from './layout/layout.component';
import { EditComponent } from './edit/edit.component';
import { SideMenuComponent } from './sidemenu/sidemenu.component';
import { RouterModule } from '@angular/router';
import { Ng2ValidatorsModule } from 'angular2-validators';
import { ValidatorsPip } from './pip/ValidatorsPip';
import { IsNotNullValidator } from './directive/validators/is-note-null';
import { Broadcaster } from './service/broadcaster';
import { DialogModule } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';


@NgModule({
    imports: [ButtonModule, GrowlModule, InputTextModule, DialogModule, AccordionModule, ReactiveFormsModule, Ng2ValidatorsModule, SharedModule, DataTableModule, RouterModule, CommonModule, FormsModule],
    declarations: [MapComponent, ValidatorsPip, IsNotNullValidator, ManageComopnent, LoginComponent, HeaderCompoent, LayoutCompoent, EditComponent, SideMenuComponent],
    exports: [RouterModule, ValidatorsPip, MapComponent, IsNotNullValidator, LoginComponent, ManageComopnent, Ng2ValidatorsModule, HeaderCompoent, EditComponent, LayoutCompoent, SideMenuComponent]
})

export class CoreModule {

}
