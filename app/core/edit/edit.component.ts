import {Component, Input, Output, ViewEncapsulation, EventEmitter} from '@angular/core';
import {Http} from '@angular/http';
import {DRIVERS} from '../../system/driver/driver.mock';

@Component({
    selector: 'be-edit',
    styleUrls:['dist/app/core/edit/edit.component.css'],
    templateUrl: 'dist/app/core/edit/edit.component.html'
})

export class EditComponent {
    @Input() subject = null;// 对象名称
    @Output() subjectChange = new EventEmitter<any>();
    
    constructor(private http: Http) {
    }

    // 构建一个 valid controller
}