import { Directive } from '@angular/core';
import { NG_VALIDATORS, AbstractControl } from '@angular/forms';

const name = 'isNotNull';
export function validate(name: string) {
  return (c: AbstractControl) => {
    if (c.value) {
      return null;
    } else {
      return {
        isNotNull: {
          valid: false
        }
      }
    }
  }
}

export const isNotNull = validate(name)

@Directive({
  selector: `[${name}][formControlName],[${name}][formControl],[${name}][ngModel]`,
  providers: [{
    provide: NG_VALIDATORS,
    useValue: validate(name),
    multi: true
  }]
})
export class IsNotNullValidator { }

// TODO figure out how to get json params