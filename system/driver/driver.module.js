/*
配置功能模块
*/
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var editor_component_1 = require('./editor.component');
var manager_component_1 = require('./manager.component');
var driver_component_1 = require('./driver.component');
var driver_routing_1 = require('./driver.routing');
var primeng_1 = require('primeng/primeng');
var core_module_1 = require('../../core/core.module');
var forms_1 = require('@angular/forms');
var router_1 = require('@angular/router');
var DriverModule = (function () {
    function DriverModule() {
    }
    DriverModule = __decorate([
        core_1.NgModule({
            imports: [driver_routing_1.driverRouting, primeng_1.RadioButtonModule, router_1.RouterModule, forms_1.ReactiveFormsModule, common_1.CommonModule, primeng_1.CalendarModule, core_module_1.CoreModule, primeng_1.ButtonModule, primeng_1.InputTextModule, primeng_1.DataTableModule, primeng_1.SharedModule, primeng_1.FieldsetModule, primeng_1.DropdownModule, forms_1.FormsModule],
            declarations: [editor_component_1.DriverEditorComponent, manager_component_1.DriverManagerComponent, driver_component_1.DriverComponent],
            exports: []
        }), 
        __metadata('design:paramtypes', [])
    ], DriverModule);
    return DriverModule;
}());
exports.DriverModule = DriverModule;

//# sourceMappingURL=driver.module.js.map

//# sourceMappingURL=driver.module.js.map
