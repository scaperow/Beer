"use strict";
var router_1 = require('@angular/router');
var editor_component_1 = require('./editor.component');
var manager_component_1 = require('./manager.component');
var driver_component_1 = require('./driver.component');
var routing = [
    {
        path: '',
        component: driver_component_1.DriverComponent,
        children: [
            {
                path: 'editor',
                component: editor_component_1.DriverEditorComponent,
                pathMatch: 'full'
            },
            {
                path: 'manager',
                component: manager_component_1.DriverManagerComponent,
                pathMatch: 'full'
            }
        ]
    }
];
exports.driverRouting = router_1.RouterModule.forChild(routing);

//# sourceMappingURL=driver.routing.js.map

//# sourceMappingURL=driver.routing.js.map
