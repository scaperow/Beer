"use strict";
var router_1 = require('@angular/router');
var monitor_component_1 = require('./monitor.component');
var maps_component_1 = require('./maps.component');
var routing = [
    {
        path: '',
        component: maps_component_1.MapsComponent,
        children: [
            {
                path: 'monitor',
                component: monitor_component_1.MapMonitorComponent,
                pathMatch: 'full'
            }
        ]
    }
];
exports.mapRouting = router_1.RouterModule.forChild(routing);

//# sourceMappingURL=map.routing.js.map

//# sourceMappingURL=map.routing.js.map
