/*
配置功能模块
*/
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var map_routing_1 = require('./map.routing');
var common_1 = require('@angular/common');
var primeng_1 = require('primeng/primeng');
var forms_1 = require('@angular/forms');
var monitor_component_1 = require('./monitor.component');
var core_module_1 = require('../../core/core.module');
var maps_component_1 = require('./maps.component');
var router_1 = require('@angular/router');
var MapModule = (function () {
    function MapModule() {
    }
    MapModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule, primeng_1.ButtonModule, common_1.CommonModule, core_module_1.CoreModule, map_routing_1.mapRouting, primeng_1.DataTableModule, primeng_1.SharedModule, primeng_1.FieldsetModule, primeng_1.DropdownModule, forms_1.FormsModule],
            declarations: [monitor_component_1.MapMonitorComponent, maps_component_1.MapsComponent],
            exports: []
        }), 
        __metadata('design:paramtypes', [])
    ], MapModule);
    return MapModule;
}());
exports.MapModule = MapModule;

//# sourceMappingURL=map.module.js.map

//# sourceMappingURL=map.module.js.map
