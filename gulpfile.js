
const less = require('gulp-less');
const path = require('path');
const watch = require('gulp-watch');
const gulp = require("gulp");
const del = require("del");
const tsc = require("gulp-typescript");
const sourcemaps = require('gulp-sourcemaps');
const tsProject = tsc.createProject("tsconfig.json");
const tslint = require('gulp-tslint');

/**
 * Remove dist directory.
 */
gulp.task('clean', (cb) => {
    return del(["dist"], cb);
}); 
 
/**
 * Lint all custom TypeScript files.
 */
gulp.task('tslint', () => {
    return gulp.src("app/**/*.ts")
        .pipe(tslint({
            formatter: 'prose'
        }))
        .pipe(tslint.report());
});

/**
 * Compile TypeScript sources and create sourcemaps in dist directory.
 */
gulp.task("compile", () => {
    var tsResult = gulp.src('app/**/*.ts')
      .pipe(sourcemaps.init())
        .pipe(tsProject());

     
    return tsResult
        .js
        .pipe(gulp.dest("dist/app"));
});

gulp.task("compile-lib", () => {
     var tsResult = gulp.src('node_modules/angular2-validators/**/*.ts')
        .pipe(sourcemaps.init())
        .pipe(tsProject());


    return    tsResult
        .js
        .pipe(gulp.dest("dist/app"));
});

/**
 * Copy all resources that are not TypeScript files into dist directory.
 */
gulp.task("resources", () => {
    let ts = gulp.src(["app/**/*", "!**/*.ts"])
        .pipe(gulp.dest("dist/app"))

    let main = gulp.src(["index.html", "systemjs.config.js"])
        .pipe(gulp.dest("dist"));

    return ts && main;
});

/**
 * Compile all less file to css 
 */
gulp.task('less', function () {
    return gulp.src('app/**/*.less')
        .pipe(less())
        .pipe(gulp.dest("dist/app")); 
});

/**
 * Copy all required libraries into dist directory.
 */
gulp.task("libs", () => {
    return gulp.src([
        'underscore/**',
        'core-js/client/shim.min.js',
        'systemjs/dist/system-polyfills.js',
        'systemjs/dist/system.src.js',
        'reflect-metadata/Reflect.js',
        'rxjs/**',
        'zone.js/dist/**',
        '@angular/**',
        'validator/**',
        'angular2-validators/**',
        'primeng/**',
        'angular2-in-memory-web-api/**'
    ], { cwd: "node_modules/**" }) /* Glob required here. */
        .pipe(gulp.dest("dist/app/lib"));
});

/**
 * Watch for changes in TypeScript, HTML and CSS files.
 */
gulp.task('watch', function () {
    gulp.watch(["app/**/*.ts"], ['compile']).on('change', function (e) {
        console.log('TypeScript file ' + e.path + ' has been changed. Compiling.');
    });
    gulp.watch(["app/**/*.less"], ['less']).on('change', function (e) {
        console.log('Less file ' + e.path + ' has been changed. Compiling.');
    });
    gulp.watch(["app/**/*.html","systemjs.config.js", "tsconfig.json", "index.html"], ['resources']).on('change', function (e) {
        console.log('Resource file ' + e.path + ' has been changed. Updating.');
    });
});

/**
 * dist the project.
 */
gulp.task("build", ['compile', 'resources', 'libs', 'less'], () => {
    console.log("disting the project ...");
});






