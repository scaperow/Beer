/**
 * System configuration for Angular 2 samples
 * Adjust as necessary for your application needs.
 */
(function (global) {
  System.config({
    paths: {
      // paths serve as alias
      'npm:': 'dist/app/lib/'
    },
    // map tells the System loader where to look for things
    map: { 
      // our app is within the app folder
      //app: 'app',
      app: 'dist/app',
      // angular bundles
      '@angular/core': 'npm:@angular/core/bundles/core.umd.js',
      '@angular/common': 'npm:@angular/common/bundles/common.umd.js',
      '@angular/compiler': 'npm:@angular/compiler/bundles/compiler.umd.js',
      '@angular/platform-browser': 'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
      '@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
      '@angular/http': 'npm:@angular/http/bundles/http.umd.js',
      '@angular/router': 'npm:@angular/router/bundles/router.umd.js',
      '@angular/forms': 'npm:@angular/forms/bundles/forms.umd.js',
      // other libraries
      'rxjs': 'npm:rxjs',
      'angular2-in-memory-web-api': 'npm:angular2-in-memory-web-api',
      'primeng': 'npm:primeng',
      'angular2-validators': 'npm:angular2-validators',
      'validator': 'npm:validator',
      'underscore': 'npm:underscore/underscore.js',
      'angular2-rest': 'npm:angular2-rest',
      // detection the browser's language' for i18n
      "i8n": "app/core/i18n/" + navigator.language || navigator.browserLanguage || 'zh-CN' + ".js"
    },
    meta: {
      'BMap': {
        scriptLoad: true,
        format: 'global',
        global: true
      },
      'i8n': {

      }
    },
    // packages tells the System loader how to load when no filename and/or no extension
    packages: {
      app: {
        main: './main.js',
        defaultExtension: 'js'
      },
      'angular2-rest': {
        defaultExtension: 'js',
        main: './angular2-rest.js'
      },
      rxjs: {
        defaultExtension: 'js'
      },
      'angular2-in-memory-web-api': {
        main: './index.js',
        defaultExtension: 'js'
      },
      'angular2-validators': {
        defaultExtension: 'js',
        main: './index.js'
      },
      'validator': {
        defaultExtension: 'js',
        main: './index.js'
      },
      'primeng': { defaultExtension: 'js' }
    }
  });
})(this);
